'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: "'https://ordinocustomerwin.azurewebsites.net/api/v1'",
  TIMEOUT: "0"
})
