// import Vue from 'vue'
import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCjrNIo4FEnUtJkcL8sxnu90NdxJ1cnlQc',
    libraries: 'places'
  }
})

const location = JSON.parse(localStorage.getItem('user-location')) || {
  lat: 0,
  lng: 0,
  name: '',
  isGeolocation: false,
  noAccess: true,
  displayAlert: false
}

const state = {
  location
}

const getters = {
  getLocation: state => {
    return state.location
  }
}

const actions = {
  setLocation (context, payload) {
    if (payload.placeId !== '0') {
      Vue.$gmapApiPromiseLazy().then(() => {
        // eslint-disable-next-line
        var geocoder = new google.maps.Geocoder()
        geocoder.geocode({'placeId': payload.placeId}, function (results, status) {
          context.commit('setLocation', {
            location: {
              lat: results[0].geometry.location.lat(),
              lng: results[0].geometry.location.lng(),
              name: results[0]['address_components'][0]['long_name'],
              isGeolocation: false,
              noAccess: false
            }
          })
        })
      })
    } else {
      if (window.location.protocol === 'file:' || window.location.port === '3000') {
        cordovaGetLocation()
          .then((position) => {
            context.commit('setLocation', {
              location: {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                name: null,
                isGeolocation: true,
                noAccess: false
              }
            })
          })
          .catch(() => {
            context.commit('displayLocationAlert')
          })
      } else {
        getGeolocation()
          .then((position) => {
            context.commit('setLocation', {
              location: {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                name: null,
                isGeolocation: true,
                noAccess: false
              }
            })
          })
          .catch(() => {
            context.commit('displayLocationAlert')
          })
      }
    }
  },
  hideLocationAlert (context) {
    context.commit('hideLocationAlert')
  }
}

const mutations = {
  setLocation (state, { location }) {
    state.location = location
    localStorage.setItem('user-location', JSON.stringify(location))
  },
  displayLocationAlert (state) {
    state.location = { displayAlert: true }
  },
  hideLocationAlert (state) {
    state.location = { displayAlert: false }
  }
}

export default {
  state,
  actions,
  getters,
  mutations
}

function getGeolocation () {
  var promise = new Promise(function (resolve, reject) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          resolve(position)
        },
        () => {
          reject(new Error('Geolocation not found'))
        },
        {
          timeout: 6000
        }
      )
    } else {
      reject(new Error('Geolocation not granted'))
    }
  })

  return promise
}

function cordovaGetLocation () {
  var promise = new Promise(function (resolve, reject) {
    Vue.cordova.on('deviceready', () => {
      if (Vue.cordova.geolocation) {
        Vue.cordova.geolocation.getCurrentPosition(
          (position) => {
            resolve(position)
          },
          () => {
            reject(new Error('Geolocation not found'))
          },
          {
            timeout: 3000
          }
        )
      } else {
        reject(new Error('Geolocation not granted'))
      }
    })
  })

  return promise
}
