import axios from 'axios'
import router from '@/router'

const state = {
  shoppingcart: null,
  pointOfSalesId: null,
  paymentUrl: null
}

const getters = {
  getArticlesNumber: state => {
    var articlesNumber = 0

    if (state.shoppingcart !== null) {
      state.shoppingcart.lines.forEach(function (e) {
        articlesNumber += e.quantity
      })
    }

    return articlesNumber
  },
  getLines: state => {
    if (state.shoppingcart) {
      return state.shoppingcart.lines
    } else {
      return null
    }
  },
  getTotalPrice: state => {
    var totalPrice = 0

    if (state.shoppingcart !== null) {
      state.shoppingcart.lines.forEach(function (e) {
        totalPrice += e.amount
      })
    }

    return totalPrice
  },
  getTotalPriceWithDiscount: state => {
    var totalPrice = 0

    if (state.shoppingcart !== null) {
      state.shoppingcart.lines.forEach(function (e) {
        totalPrice += e.amount
        if (e.discount) {
          totalPrice += e.discount.amount * e.quantity
        }
      })
    }

    return totalPrice
  },
  getPaymentUrl: state => {
    return state.paymentUrl
  },
  getQuantityInCart: state => articleId => {
    if (state.shoppingcart !== null) {
      var line = state.shoppingcart.lines.find(line => line.articleId === articleId)

      if (line !== undefined) {
        return line['quantity']
      } else {
        return 0
      }
    } else {
      return 0
    }
  },
  getOrderLineId: state => articleId => {
    var line = state.shoppingcart.lines.find(line => line.articleId === articleId)

    return line['orderLineId']
  },
  getTodayUnavailableArticles: state => {
    var unavailableArticles = state.shoppingcart.lines.filter(
      line => line.quantity > line.todayStockQuantity - 2
    )

    return unavailableArticles
  }
}

const actions = {
  loadCart (context, payload) {
    if (payload.pointOfSalesId) {
      context.commit('setPointOfSalesId', payload.pointOfSalesId)
    }

    axios
      .get(process.env.API_URL + '/PointsOfSales/' + state.pointOfSalesId + '/CurrentOrder')
      .then((response) => {
        const shoppingCart = response.data.result
        const pointOfSalesId = state.pointOfSalesId
        context.commit('loadCart', { shoppingCart, pointOfSalesId })
      })
      .catch(e => {
        alert(e)
      })
  },
  addToCart (context, payload) {
    axios
      .post(process.env.API_URL + '/PointsOfSales/' + payload.pointOfSalesId + '/CurrentOrder/Lines', {
        articleId: payload.articleId,
        quantity: payload.quantity,
        menuChoices: payload.menuChoices
      })
      .then(response => {
        context.dispatch('loadCart', payload)
      })
      .catch(e => {
        alert(e)
      })
  },
  changeQuantity (context, payload) {
    axios
      .patch(process.env.API_URL + '/PointsOfSales/' +
        state.pointOfSalesId + '/CurrentOrder/Lines/' + payload.orderLineId, {
        newQuantity: payload.newQuantity
      })
      .then(response => {
        context.dispatch('loadCart', payload)
      })
      .catch(e => {
        alert(e)
      })
  },
  removeFromCart (context, payload) {
    axios
      .delete(process.env.API_URL + '/PointsOfSales/' +
        state.pointOfSalesId + '/CurrentOrder/Lines/' + payload.orderLineId)
      .then(response => {
        context.dispatch('loadCart', payload)
      })
      .catch(e => {
        alert(e)
      })
  },
  sendCart (context, payload) {
    axios
      .patch(process.env.API_URL + '/PointsOfSales/' +
        payload.pointOfSalesId + '/CurrentOrder/', {
        withdrawalDate: payload.withdrawnDate
      })
      .then(response => {
        axios
          .post(process.env.API_URL + '/PointsOfSales/' +
            payload.pointOfSalesId + '/CurrentOrder/InitializePayment/')
          .then(response => {
            context.commit('setPaymentUrl', response.data.result.url)
            router.push({name: 'paymentform'})
          })
          .catch(e => {
            alert(e)
          })
      })
      .catch(e => {
        alert(e)
      })
  }
}

const mutations = {
  loadCart (state, { shoppingCart, pointOfSalesId }) {
    state.pointOfSalesId = pointOfSalesId
    state.shoppingcart = shoppingCart
  },
  setPointOfSalesId (state, pointOfSalesId) {
    state.pointOfSalesId = pointOfSalesId
  },
  setPaymentUrl (state, url) {
    state.paymentUrl = url
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
