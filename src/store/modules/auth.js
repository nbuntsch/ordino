import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT, AUTH_TMP_USER } from '../actions/auth'
import axios from 'axios'
import { Trans } from '@/plugins/i18n'

const state = {
  token: localStorage.getItem('user-token') || '',
  language: localStorage.getItem('language') || '',
  status: '',
  tmpUser: null
}

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  tmpUser: state => state.tmpUser
}

const actions = {
  [AUTH_REQUEST]: ({commit, dispatch}, user) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST)
      axios
        .post(process.env.API_URL + '/Users/authenticate', {
          email: user.email,
          password: user.password
        })
        .then(resp => {
          const token = resp.data.result.token
          localStorage.setItem('user-token', token)
          const language = resp.data.result.language.toLowerCase()
          console.log(language)
          // const language = 'fr'
          Trans.changeLanguage(language)

          axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
          commit(AUTH_SUCCESS, token)
          resolve(resp)
        })
        .catch(err => {
          commit(AUTH_ERROR, err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  [AUTH_LOGOUT]: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT)
      localStorage.removeItem('user-token')
      delete axios.defaults.headers.common['Authorization']
      resolve()
    })
  },
  [AUTH_TMP_USER]: ({commit, dispatch}, user) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_TMP_USER, user)
      resolve()
    })
  }
}

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [AUTH_SUCCESS]: (state, token, language) => {
    state.status = 'success'
    state.token = token
    state.language = language
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = ''
  },
  [AUTH_TMP_USER]: (state, user) => {
    state.tmpUser = user
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
