const state = {
  distanceMaxInKm: 50,
  onlyOpenNow: false,
  category: [],
  sortBy: 'DistanceInKm',
  direction: 'ASC'
}

const getters = {
  getFilters: state => {
    return Object.assign({}, state)
  }
}

const actions = {
  setFilters (context, payload) {
    context.commit('setFilters', { data: payload })
  }
}

const mutations = {
  setFilters (state, { data }) {
    for (var key in data) {
      state[key] = data[key]
    }
  }
}

export default {
  state,
  actions,
  getters,
  mutations
}
