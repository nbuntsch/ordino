import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import filter from './modules/filter'
import location from './modules/location'
import shoppingcart from './modules/shoppingcart'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({
  modules: {
    auth,
    filter,
    shoppingcart,
    location
  },
  strict: debug
})

export default store
