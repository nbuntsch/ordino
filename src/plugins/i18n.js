
import VueI18n from 'vue-i18n'
import Vue from 'vue'
import fr from '@/lang/fr.json'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'fr',
  messages: { fr }
})

export const Trans = {
  changeLanguage (language) {
    return Trans.loadLanguageFile(language).then(messages => {
      i18n.setLocaleMessage(language, messages.default || messages)
      i18n.locale = language
      localStorage.setItem('user-language', language)

      const moment = require('moment')
      require('moment/locale/' + language)

      Vue.use(require('vue-moment'), {
        moment
      })
    })
  },
  loadLanguageFile (language) {
    return import(`@/lang/${language}.json`)
  }
}

const language = localStorage.getItem('user-language') || 'fr'
Trans.changeLanguage(language)
