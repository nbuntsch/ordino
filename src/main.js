// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios'
import router from './router'
import store from './store'
import VueCordova from 'vue-cordova'
import { i18n } from './plugins/i18n'
import Vuetify from 'vuetify'
import './stylus/main.styl'
import VueMarkdown from 'vue-markdown'
const moment = require('moment')
require('moment/locale/fr')

Vue.use(require('vue-moment'), {
  moment
})

Vue.use(VueCordova)
Vue.use(Vuetify, {
  theme: {
    primary: '#EB823B',
    secondary: '#b0bec5',
    accent: '#8c9eff',
    error: '#b71c1c'
  }
})

Vue.component('vue-markdown', VueMarkdown)

Vue.config.productionTip = false

if (window.location.protocol === 'file:' || window.location.port === '3000') {
  var cordovaScript = document.createElement('script')
  cordovaScript.setAttribute('type', 'text/javascript')
  cordovaScript.setAttribute('src', 'cordova.js')
  document.body.appendChild(cordovaScript)
}

const token = localStorage.getItem('user-token')
if (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

Vue.filter('distance', function (value) {
  if (value > 5) {
    return value.toFixed(0) + ' km'
  } else if (value > 1) {
    return value.toFixed(1) + ' km'
  } else {
    return (value * 1000).toFixed(0) + ' m'
  }
})

Vue.filter('price', function (value) {
  if (Number.isInteger(value)) {
    return 'CHF ' + value + '.-'
  } else {
    return 'CHF ' + value.toFixed(2)
  }
})

Vue.filter('listPrice', (value) => {
  return value.toFixed(2)
})

Vue.filter('date', function (value) {
  return new Date(value).toLocaleDateString('fr-ch', {
    month: 'numeric', day: 'numeric', year: 'numeric' })
})

Vue.filter('datetime', function (value) {
  return new Date(value).toLocaleDateString('fr-ch', {
    weekday: 'long', day: 'numeric', month: 'numeric', year: 'numeric'}) + ' - ' +
    new Date(value).toLocaleTimeString('fr-ch', {hour: 'numeric', minute: 'numeric'})
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  store,
  components: { App, VueMarkdown },
  template: '<App/>'/* ,
  created () {
    this.$store.dispatch('setLocation', {placeId: '0'})
  } */
})
