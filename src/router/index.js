import Vue from 'vue'
import Router from 'vue-router'
import CreateAccountForm from '@/views/CreateAccountForm'
import HomeList from '@/views/HomeList'
import LoginForm from '@/views/LoginForm'
import MenuForm from '@/views/MenuForm'
import OrderConfirmation from '@/views/OrderConfirmation'
import OrderDetail from '@/views/OrderDetail'
import OrdersList from '@/views/OrdersList'
import PaymentForm from '@/views/PaymentForm'
import PaymentInfoForm from '@/views/PaymentInfoForm'
import PointsOfSalesList from '@/views/PointsOfSalesList'
import PointsOfSalesFilters from '@/views/PointsOfSalesFilters'
import PointsOfSalesMap from '@/views/PointsOfSalesMap'
import PointsOfSalesSort from '@/views/PointsOfSalesSort'
import PointOfSalesDetail from '@/views/PointOfSalesDetail'
import PointOfSalesArticles from '@/views/PointOfSalesArticles'
import PresentationSlider from '@/views/PresentationSlider'
import ShoppingCartDetail from '@/views/ShoppingCartDetail'
import UserForm from '@/views/UserForm'
import ValidateAccountForm from '@/views/ValidateAccountForm'
import WithdrawnForm from '@/views/WithdrawnForm'
import store from '@/store'

Vue.use(Router)

/* const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
} */

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeList,
      beforeEnter: ifAuthenticated,
      meta: {
        level: 1
      }
    },
    {
      path: '/pointsofsales/:category?',
      name: 'pointsofsales',
      component: PointsOfSalesList,
      meta: {
        level: 2
      }
    },
    {
      path: '/pointsofsalesfilters',
      name: 'pointsofsalesfilters',
      component: PointsOfSalesFilters,
      meta: {
        level: 3
      }
    },
    {
      path: '/pointsofsalesmap',
      name: 'pointsofsalesmap',
      component: PointsOfSalesMap,
      meta: {
        level: 3
      }
    },
    {
      path: '/pointsofsalessort',
      name: 'pointsofsalessort',
      component: PointsOfSalesSort,
      meta: {
        level: 3
      }
    },
    {
      path: '/pointofsales/:id/:home?',
      name: 'pointofsales',
      component: PointOfSalesDetail,
      props: { id: null, home: false },
      meta: {
        level: 3
      }
    },
    {
      path: '/pointofsales/:pointOfSalesId/articles/:articleCategoryId',
      name: 'pointofsalesarticles',
      component: PointOfSalesArticles,
      props: { pointOfSalesId: null, articleCategoryId: null },
      meta: {
        level: 4
      }
    },
    {
      path: '/menu/:pointOfSalesId/:categoryId/:articleId',
      name: 'menu',
      component: MenuForm,
      props: { pointOfSalesId: null, categoryId: null, articleId: null },
      meta: {
        level: 5
      }
    },
    {
      path: '/shoppingcart/:pointOfSalesId/:showTodayUnavailable?',
      name: 'shoppingcart',
      component: ShoppingCartDetail,
      props: { pointOfSalesId: null, showTodayUnavailable: false },
      meta: {
        level: 5
      }
    },
    {
      path: '/withdrawn/:pointOfSalesId',
      name: 'withdrawn',
      component: WithdrawnForm,
      props: { pointOfSalesId: null },
      meta: {
        level: 6
      }
    },
    {
      path: '/paymentform',
      name: 'paymentform',
      component: PaymentForm,
      meta: {
        level: 6
      }
    },
    {
      path: '/orderconfirmation/:orderId',
      name: 'orderconfirmation',
      component: OrderConfirmation,
      props: { orderId: null },
      meta: {
        level: 7
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginForm
    },
    {
      path: '/createaccount',
      name: 'createaccount',
      component: CreateAccountForm
    },
    {
      path: '/validateaccount',
      name: 'validateaccount',
      component: ValidateAccountForm
    },
    {
      path: '/presentation',
      name: 'presentation',
      component: PresentationSlider
    },
    {
      path: '/orders/:status',
      name: 'orders',
      component: OrdersList,
      props: { status: null }
    },
    {
      path: '/orderdetail/:orderId',
      name: 'orderdetail',
      component: OrderDetail,
      props: { orderId: null }
    },
    {
      path: '/payment',
      name: 'payment',
      component: PaymentInfoForm
    },
    {
      path: '/account',
      name: 'account',
      component: UserForm
    }
  ]
})
