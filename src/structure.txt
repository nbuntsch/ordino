** Shopping Workflow **

* level 1 *

HomeList
-- HomeGroupSection
   -- homeGroupCategoryCard
   -- homeGroupPointOfSalesCard
-- TheGeolocationAlert
-- TheLocationSelector
-- ThePointsOfSalesSearchInput
-- TheUserMenu

* level 2 *

PointsOfSalesList
-- PointOfSalesCard

PointsOfSalesFilter

PointsOfSalesSort

PointsOfSalesMap
-- pointsOfSalesMapDetail
   -- PointOfSalesCard

* level 3 *

PointOfSalesDetail
-- ArticleCategoriesList
   -- ArticleCategoryTile
-- ArticleHighlightList
   -- ArticleCard
      -- MenuForm
         -- MenuFormGroup
-- TheShoppingCartButton

* level 4 *

ShoppingCartDetail
-- shoppingCartLine

* level 5 *

OrderConfirmation


** Login Workflow **

* level 1 *

LoginForm 

CreateAccountForm

* level 2 *

PresentationSlider
